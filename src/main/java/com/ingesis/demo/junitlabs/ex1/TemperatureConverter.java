package com.ingesis.demo.junitlabs.ex1;

import java.math.BigDecimal;

public class TemperatureConverter {
    private static BigDecimal CONVERT_FACTOR = new BigDecimal(1.8).setScale(10, BigDecimal.ROUND_HALF_UP);
    private static BigDecimal CONVERT_ADDEND = new BigDecimal(32).setScale(5);

    public BigDecimal toFahrenheit(BigDecimal celsius) {
        return celsius
                .multiply(CONVERT_FACTOR)
                .add(CONVERT_ADDEND).setScale(5, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal toCelsius(BigDecimal fahrenheit) {
        return fahrenheit
                .divide(CONVERT_FACTOR,5, BigDecimal.ROUND_HALF_UP)
                .subtract(CONVERT_ADDEND);
    }
}
