package com.ingesis.demo.junitlabs.ex2;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExchangeRate {
    private LocalDate date;
    private String currencyCode;
    private BigDecimal exchangeRate;

    public ExchangeRate(LocalDate date, String currencyCode, BigDecimal exchangeRate) {
        this.date = date;
        this.currencyCode = currencyCode;
        this.exchangeRate = exchangeRate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
