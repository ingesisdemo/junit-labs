package com.ingesis.demo.junitlabs.ex2;


import java.math.BigDecimal;
import java.time.LocalDate;

public class ExchangeRateService {
    private ExchangeRateLogic exchangeRateLogic;

    public ExchangeRateService(ExchangeRateLogic exchangeRateLogic) {
        this.exchangeRateLogic = exchangeRateLogic;
    }

    public BigDecimal lowestExchangeRate(String currencyCode, LocalDate fromDate, LocalDate toDate) {
        return exchangeRateLogic.lowestExchangeRate(currencyCode, fromDate, toDate);
    }

}
