package com.ingesis.demo.junitlabs.ex2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.ingesis.demo.junitlabs.ex2.ExchangeRateConstants.USD;

public class ExchangeRateDao {
    private static List<ExchangeRate> exchangeRates = new ArrayList<>();

    static {
        exchangeRates.add(new ExchangeRate(LocalDate.of(2020, 3, 1),
                USD, new BigDecimal(7.75)));
        exchangeRates.add(new ExchangeRate(LocalDate.of(2020, 3, 5),
                USD, new BigDecimal(7.82)));
        exchangeRates.add(new ExchangeRate(LocalDate.of(2020, 3, 12),
                USD, new BigDecimal(7.62)));
        exchangeRates.add(new ExchangeRate(LocalDate.of(2020, 3, 21),
                USD, new BigDecimal(8.01)));
    }

    /**
     * Suppose to be a query to access database
     *
     * @param currencyCode
     * @param fromDate
     * @param toDate
     * @return
     */
    public List<ExchangeRate> findBetween(String currencyCode, LocalDate fromDate, LocalDate toDate) {
        return exchangeRates;
    }

}
