package com.ingesis.demo.junitlabs.ex2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;

public class ExchangeRateLogic {
    private ExchangeRateDao exchangeRateDao;

    public ExchangeRateLogic(ExchangeRateDao exchangeRateDao) {
        this.exchangeRateDao = exchangeRateDao;
    }

    public BigDecimal lowestExchangeRate(String currencyCode, LocalDate fromDate, LocalDate toDate) {
        return exchangeRateDao.findBetween(currencyCode, fromDate, toDate).stream()
                .map(ExchangeRate::getExchangeRate)
                .min(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);
    }

}
