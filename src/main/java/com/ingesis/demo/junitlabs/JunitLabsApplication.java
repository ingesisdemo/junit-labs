package com.ingesis.demo.junitlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitLabsApplication {

    public static void main(String[] args) {
        SpringApplication.run(JunitLabsApplication.class, args);
    }

}
